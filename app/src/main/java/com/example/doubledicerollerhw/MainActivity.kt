package com.example.doubledicerollerhw

/**
 * Alper HIZ 20200808075
 */
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollDice()
        }
    }

    /**
    rolling dice and update the dices
     */
    private fun rollDice() {
        val dice1 = Dice(numSides = 10)
        val dice2 = Dice(numSides = 10)

        val diceRoll = dice1.roll()
        val diceRoll2 = dice2.roll()

        val diceImage: ImageView = findViewById(R.id.imageView)
        val diceImage2: ImageView = findViewById(R.id.imageView2)

        val drawableResource = when (diceRoll) {
            1 -> R.drawable.kismet_one
            2 -> R.drawable.kismet_two
            3 -> R.drawable.kismet_three
            4 -> R.drawable.kismet_four
            5 -> R.drawable.kismet_five
            else -> R.drawable.kismet_six
        }
        diceImage.setImageResource(drawableResource)
        val drawableResource2 = when (diceRoll2) {
            1 -> R.drawable.kismet_one
            2 -> R.drawable.kismet_two
            3 -> R.drawable.kismet_three
            4 -> R.drawable.kismet_four
            5 -> R.drawable.kismet_five
            else -> R.drawable.kismet_six
        }
        diceImage2.setImageResource(drawableResource2)
 // just for the commit test
    }
}

/**
to create dice and create dice value
 */

class Dice(val numSides: Int) {
    fun roll(): Int {
        return (1..numSides).random()
    }
}